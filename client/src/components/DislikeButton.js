import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useMutation } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import { Button, Label, Icon } from 'semantic-ui-react';

import MyPopup from '../util/MyPopup';

function DislikeButton({ user, post: { id, dislikeCount, dislikes } }) {
    const [disliked, setDisliked] = useState(false);

    useEffect(() => {
        if (user && dislikes.find((dislike) => dislike.username === user.username)) {
            setDisliked(true);
        } else setDisliked(false);
    }, [user, dislikes]);

    const [dislikePost] = useMutation(DISLIKE_POST_MUTATION, {
        variables: { postId: id },
    });

    const dislikeButton = user ? (
        disliked ? (
            <Button color="red">
                <Icon name="heart" />
            </Button>
        ) : (
            <Button color="red" basic>
                <Icon name="heart" />
            </Button>
        )
    ) : (
        <Button as={Link} to="/login" color="teal" basic>
            <Icon name="heart" />
        </Button>
    );

    return (
        <Button as="div" labelPosition="right" onClick={dislikePost}>
            <MyPopup content={disliked ? 'Undo Dislike' : 'Dislike'}>{dislikeButton}</MyPopup>
            <Label basic color="red" pointing="left">
                {dislikeCount}
            </Label>
        </Button>
    );
}

const DISLIKE_POST_MUTATION = gql`
    mutation dislikePost($postId: ID!) {
        dislikePost(postId: $postId) {
            id
            dislikes {
                id
                username
            }
            dislikeCount
        }
    }
`;

export default DislikeButton;
